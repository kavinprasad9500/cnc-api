<?php

${basename(__FILE__, '.php')} = function () {
    if ($this->get_request_method() == "POST"  and isset($this->_request['product_name']) and isset($this->_request['process']) and isset($this->_request['machine_name'])) {
        try {

            $f = new Product();
            if($f->createNew($this->_request['product_name'],$this->_request['process'], $this->_request['machine_name']))
                $data = [
                    "Message" => "Success"
                ];
            $data = $this->json($data);
            $this->response($data, 200);
        } catch (Exception $e) {
            $data = [
                "error" => $e->getMessage() 
            ];
            $data = $this->json($data);
            $this->response($data, 403);
        }
    } else {
        $data = [
            "error" => "Bad request"
        ];
        $data = $this->json($data);
        $this->response($data, 400);
    }
};
