<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
${basename(__FILE__, '.php')} = function () {
        try {
            $p = new Product();
            $data = $p->getAllProducts();
            $data = $this->json($data);
            $this->response($data, 200);
        } catch (Exception $e) {
            $data = [
                "error" => $e->getMessage()
            ];
            $data = $this->json($data);
            $this->response($data, 403);
        }
    
};
