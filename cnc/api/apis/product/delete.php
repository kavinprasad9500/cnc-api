<?php

${basename(__FILE__, '.php')} = function () {
    if ($this->get_request_method() == "POST" and isset($this->_request['id'])) {
        try {
            $f = new Product($this->_request['id']);
            if ($f->deleteProduct()) {
                $data = [
                    'message' => 'success',
                ];
                $data = $this->json($data);
                $this->response($data, 200);
            } else {
                $data = [
                    "error" => "Cannot delete"
                ];
                $data = $this->json($data);
                $this->response($data, 400);
            }
        } catch (Exception $e) {
            $data = [
                "error" => $e->getMessage()
            ];
            $data = $this->json($data);
            $this->response($data, 403);
        }
    } else {
        $data = [
            "error" => "Bad request"
        ];
        $data = $this->json($data);
        $this->response($data, 400);
    }
};
