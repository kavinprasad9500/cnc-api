<?php

${basename(__FILE__, '.php')} = function () {
    if (isset($this->_request['id'])) {
        try {
            $n = new Process($this->_request['id']);
            $data = $n->getProduct();
            $data = $this->json($data);
            $this->response($data, 200);
        } catch (Exception $e) {
            $data = [
                "error" => $e->getMessage()
            ];
            $data = $this->json($data);
            $this->response($data, 403);
        }
    } else {
        $data = [
            "error" => "Bad request"
        ];
        $data = $this->json($data);
        $this->response($data, 400);
    }
};
