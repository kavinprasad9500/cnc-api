<?php

${basename(__FILE__, '.php')} = function(){
    if ($this->get_request_method() == "POST" and isset($this->_request['student_id'])) {
        try {
            $f = new Product($this->_request['student_id']);
            if($f->sendEmail()){
                $data = [
                    "message" => "Success"
                ];
                $data = $this->json($data);
                $this->response($data, 200);
            }
        } catch(Exception $e){
            $data = [
                "error" => $e->getMessage()
            ];
            $data = $this->json($data);
            $this->response($data, 403);
        }

    } else {
        $data = [
            "error" => "Bad request"
        ];
        $data = $this->json($data);
        $this->response($data, 400);
    }
};
