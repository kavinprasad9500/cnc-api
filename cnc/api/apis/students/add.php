<?php

${basename(__FILE__, '.php')} = function () {
    if ($this->get_request_method() == "POST"  and isset($this->_request['student_name']) and isset($this->_request['process'])) {
        try {

            $n = new Students();
            if($n->createNew($this->_request['student_name'], $this->_request['process']))
            $data = [
                "Message" => "Success"
            ];
            $data = $this->json($data);
            $this->response($data, 200);
        } catch (Exception $e) {
            $data = [
                "error" => $e->getMessage()
            ];
            $data = $this->json($data);
            $this->response($data, 403);
        }
    } else {
        $data = [
            "error" => "Bad request"
        ];
        $data = $this->json($data);
        $this->response($data, 400);
    }
};
