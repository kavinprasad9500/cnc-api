<?php

${basename(__FILE__, '.php')} = function () {
    if (isset($this->_request['id'])) {
        try {

            $n = new Students($this->_request['id']);
            if($n->deleteStudents()){
                $data = [
                    "message" => "Success"
                ];
                $data = $this->json($data);
                $this->response($data, 200);
            }
            else{
                $data = [
                    "error" => "Cannot delete"
                ];
                $data = $this->json($data);
                $this->response($data, 400);

            }
            
        } catch (Exception $e) {
            $data = [
                "error" => $e->getMessage()
            ];
            $data = $this->json($data);
            $this->response($data, 403);
        }
    } else {
        $data = [
            "error" => "Bad request"
        ];
        $data = $this->json($data);
        $this->response($data, 400);
    }
};
