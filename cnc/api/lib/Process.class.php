<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/api/lib/Database.class.php');

class Process
{
    private $db;
    private $data=null;
    private $id=null;
    public function __construct($id = null)
    {
        $this->db = Database::getConnection();
        if($id!=null) {
            $this->id = $id;
        }
    }


    public function getAllProcess()
    {
        $query = "SELECT * FROM `process` WHERE `product_id` = $this->id";
        $result = mysqli_query($this->db, $query);
        if($result) {
            $data = mysqli_fetch_all($result, MYSQLI_ASSOC);
            return $data;
        } else {
            return[];
        }

    }

    public function getProcess()
    {
        $query ="SELECT * FROM `process` WHERE `process_id` = $this->id";
        $result = mysqli_query($this->db, $query);
        if ($result) {
            $data = mysqli_fetch_all($result, MYSQLI_ASSOC);
            return $data;
        } else {
            return [];
        }
    }
    public function getProduct()
    {
        $query ="SELECT * FROM `products` WHERE `product_id` = $this->id";
        $result = mysqli_query($this->db, $query);
        if ($result) {
            $data = mysqli_fetch_all($result, MYSQLI_ASSOC);
            return $data;
        } else {
            return [];
        }
    }
    public function getStudent()
    {
        $query ="SELECT * FROM `students` WHERE `student_id`= $this->id";
        $result = mysqli_query($this->db, $query);
        if ($result) {
            $data = mysqli_fetch_all($result, MYSQLI_ASSOC);
            return $data;
        } else {
            return [];
        }
    }
}
