<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/api/lib/Database.class.php');

class Product
{
    private $db;
    private $data = null;
    private $id = null;

    public function __construct($id = null)
    {
        $this->db = Database::getConnection();
        $this->id = $id;
    }

    public function getAllProducts()
    {
        $query = "SELECT * FROM `products`";
        $result = mysqli_query($this->db, $query);
        if ($result) {
            $data = mysqli_fetch_all($result, MYSQLI_ASSOC);
            return $data;
        } else {
            return [];
        }
    }

    public function deleteProduct()
    {
        if ($this->id) {
            $process = "DELETE FROM `process` WHERE `product_id` = $this->id";
            mysqli_query($this->db, $process);

            $product = "DELETE FROM `products` WHERE `product_id` = $this->id";
            $results = mysqli_query($this->db, $product);
            if($results)
                return $results;
            else
                throw new Exception("Something Wrong");

        } else {
            throw new Exception("Please Select Products");
        }
    }
    
    public function rename($product_id, $product_name, $process, $machine_name){
        $query_product = "UPDATE `products` SET `product_name` = '$product_name', `machine_name` = '$machine_name' WHERE `product_id` = $product_id";
        if(mysqli_query($this->db, $query_product)){
            $jsonString = stripslashes($process); // Remove the backslashes
            $result = json_decode($jsonString, true);
            // print_r($result);
            foreach ($result as $single_process) {
                $process_id = $single_process[0];
                $process_name = $single_process[1];
                $time = $single_process[2];
                $cost = $single_process[3];
                $query_process = "UPDATE `process` SET `process_name` = '$process_name', `time` = '$time', `cost` = '$cost' WHERE `process_id` = '$process_id' AND `product_id` = '$product_id'";
                
                if (!mysqli_query($this->db, $query_process)) {
                    return false; // Return false if any update fails
                }
                // print($process_id.$process_name.$time.$cost."\n");
            }
        }
        else {
            throw new Exception("Something went wrong in product name");
            return false;
        }
        return true; // Return true if all updates are successful
    }


    public function createNew($product_name, $process, $machine_name){
        $query_product = "INSERT INTO `products` (`product_name`,`machine_name`) VALUES ('$product_name','$machine_name')";
        if(mysqli_query($this->db, $query_product)){
            $this->id = mysqli_insert_id($this->db);
                // print($this->id);
            // print_r($process);
            $jsonString = stripslashes($process); // Remove the backslashes
            $result = json_decode($jsonString, true);
            // print_r($result);
            foreach ($result as $single_process) {
                $process_name = $single_process[0];
                $time = $single_process[1];
                $cost = $single_process[2];
                $query_process = "INSERT INTO `process` (`product_id`, `process_name`, `time`, `cost`)
                VALUES ('$this->id', '$process_name', '$time', '$cost');";
                
                if (!mysqli_query($this->db, $query_process)) {
                    return false; // Return false if any update fails
                }
                // print($process_id.$process_name.$time.$cost."\n");
            }
        }
        else {
            throw new Exception("Something went wrong in product name");
        }
        return true; // Return true if all updates are successful
    }
    public function sendEmail()
    {
        if ($this->id) {
            $email = "UPDATE `students` SET `email` = '1' WHERE `student_id` = $this->id;";
            mysqli_query($this->db, $email);
            $results = mysqli_query($this->db, $email);
            if($results)
                return true;
            else
                throw new Exception("Something Wrong");

        } else {
            throw new Exception("Please Select Products");
        }
    }

    
}
?>
