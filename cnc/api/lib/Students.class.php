<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/api/lib/Database.class.php');

class Students
{
    private $db;
    private $data = null;
    private $id = null;

    public function __construct($id = null)
    {
        $this->db = Database::getConnection();
        $this->id = $id;
    }

    public function listStudents(){
        $query = "SELECT * FROM `students`";
        $result = mysqli_query($this->db, $query);
        if ($result) {
            $data = mysqli_fetch_all($result, MYSQLI_ASSOC);
            return $data;
        } else {
            return [];
        }
    }

    public function deleteStudents(){
        if ($this->id) {
            $process = "DELETE FROM `students_activites` WHERE `student_id` = $this->id";
            mysqli_query($this->db, $process);
            $product = "DELETE FROM `students` WHERE `student_id` = $this->id";
            $results = mysqli_query($this->db, $product);
            if($results)
                return $results;
            

        } else {
            throw new Exception("Please Select Students");
        }
    }
    

    public function createNew($student_name, $process){
        $query_product = "INSERT INTO `students` (`student_name`, `date`, `email`) VALUES ('$student_name', now(), 0);";
        if(mysqli_query($this->db, $query_product)){
            $this->id = mysqli_insert_id($this->db);
                // print($this->id);
            // print_r($process);
            $jsonString = stripslashes($process); // Remove the backslashes
            $result = json_decode($jsonString, true);
            // print_r($result);
            foreach ($result as $single_process) {
                $process_id = $single_process[0];
                $count = $single_process[1];
                $query_process = "INSERT INTO `students_activites` (`student_id`, `process_id`, `count`)
                VALUES ('$this->id', '$process_id', '$count');";
                
                if (!mysqli_query($this->db, $query_process)) {
                    return false; // Return false if any update fails
                }
                // print($process_id.$process_name.$time.$cost."\n");
            }
        }
        else {
            throw new Exception("Something went wrong in product name");
        }
        return true; // Return true if all updates are successful
    }

    public function listStudentsActivites(){
        $query = "SELECT * FROM `students_activites` WHERE `student_id`= $this->id";
        $result = mysqli_query($this->db, $query);
        if ($result) {
            $data = mysqli_fetch_all($result, MYSQLI_ASSOC);
            return $data;
        } else {
            return [];
        }
    }
    
}
?>
