### API Development Course by LAHTP

This code is right now accessible at: https://{yourdomain.com}/api/

Right outside the document root, create a file called `cnc-api.json` and keep the contents of the file similar to the following. 

```
{
	"database": "cnc",
	"username": "root",
	"password": "password",
	"server": "localhost"
}
```

This will be called by the API functions to get the database connection. 

This project is under development.

#### Virtual Host Apache Configuration:

```
<VirtualHost *:80>
    ServerAdmin kavinprasadj@gmail.com      
    DocumentRoot "{repository/location}"
    ServerName {yourdomain}

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

    <Directory "{repository/location}">
            Options Indexes FollowSymLinks ExecCGI Includes
            AllowOverride All
            Require all granted
    </Directory>

</VirtualHost>

```

In the above configuration, `cnc-api.json` should sit exactly `POINT OUT THE CNC FOLDER` here.

#### Configuring your own Ubuntu Setup

Reference: https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-20-04


1. Update and upgrade the system first.

```
$ sudo apt update && sudo apt -y upgrade
```

2. Install Apache, MySQL and PHP

```
$ sudo apt install apache2 libapache2-mod-php mysql-server php-mysql
```

3. Secure MySQL Database

```
$ sudo mysql_secure_installation
```

and follow the onscreen steps. For more info, check the above link.

4. Create a Database 

```
$ mysql -u root -p
Password:
```

Enter the password you have given for root during `mysql_secure_installation` and you can see the following promot.

```
mysql>
```

From here, we need to create a database called `cnc`.

```
mysql> CREATE DATABASE kavin_api;
```

We also need to create a mysql username and password and give the database previleges for the database we created.

```
mysql> CREATE USER 'apiuser'@'localhost' IDENTIFIED BY 'password';
Query OK, 0 rows affected (0.02 sec)

mysql> GRANT ALL PRIVILEGES ON * . * TO 'apiuser'@'localhost';
Query OK, 0 rows affected (0.00 sec)

mysql> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.01 sec)

mysql> exit
Bye

```

5. Now fix the file permissions for `/var/www` folder like you own it. The below command will change the owner of the foler /var/www as you, so that no errors will come when you try to edit or create.

```
$ cd /var
$ sudo chown $(whoami):$(whoami) -R www
```


6. Now import the database export locaked at `config/migrate.sql` into the database you just created and we have all the tables. 

Now update the `env.json` file with the user and database info created. All set, your code should be accessible at http://localhost or whereever you configured it to work. 

### Security

All the data that you get with `$this->_request[]` inside the APIs are secured with `mysqli_real_escape_string` during the API initialization. Look for the function called `REST::cleanInputs()` inside `api/REST.api.php` and here is where it happens. So this development is considered secured from MySQLi injections. If you access `$_GET` or `$_POST` anywhere else directly without `$this->_request[]`, then you might just need to filter the inputs yourself and make them secure.